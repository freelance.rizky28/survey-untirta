$(document).ready(function(){
  var roles,
  data = [];

    $('.roleSurviyor').on('change',function(e){
      e.preventDefault();
      var data= $(this).val();
      // console.log('data',data)
      roles = data;
      if (data == 'STUDENT') {
          $('.role-mahasiswa').css('display','block')
          $('.role-dosen').css('display','none')
          $('.role-masyarakat').css('display','none')
      } else if (data == 'LECTURER') {
        $('.role-mahasiswa').css('display','none')
        $('.role-dosen').css('display','block')
        $('.role-masyarakat').css('display','none')        
      } else if (data =='PUBLIC') {
        $('.role-mahasiswa').css('display','none')
        $('.role-dosen').css('display','none')
        $('.role-masyarakat').css('display','block')                  
      }
    });

    $(".input-number").on("keydown keyup change", function(){
      var $this = $(this),
        value = $this.val(),
        minLength = 1,
        maxLength = 10;
      if (value.length < minLength) {
        $(".alert-number").text("Tidak Boleh Kosong");
        $this.css('border-color', 'red')
        // $('.input-number').addClass('empty')
      }
      else if (value.length >= minLength && value.length < maxLength) {
        $(".alert-number").text("Min 10 digit");
        $('.input-numbers').css('border-color', 'red')
        $this.css('color', 'red')
        // $('.input-number').addClass('not-valid')
      }
      else if (value.length > maxLength) {
        $(".alert-number").text("Digit Terlalu Panjang");
        $('.input-numbers').css('border-color', 'red')
        $this.css('color', 'red')
        // $('.input-number').addClass('to-long')
      } else {
        $(".alert-number").text("");
        $('.input-numbers').css('border-color', 'rgb(204 204 204)')
        $this.css('color', 'rgb(102 102 102)')
        // $('.input-number').addClass('Valid')
      }
  });

    $('.btn-mainPage').on('click',function(e) {
        e.preventDefault();
        var $this = $('#DynamicValueAssignedHere'),
        _selectRole = $this.find('select[name="class"]').val(),
        
        _name_student = $('.student-name').val(),
        _nim_Student = $('.student-nims').val(),
        _gender_student = $('.student-gender').val(),
        _fakultas_student = $('.student-fakultas').val(),
        
        _name_lecturer = $('.lecturer-name').val(),
        _nik_lecturer = $('.lecturer-nik').val(),
        _gender_lecturer = $('.lecturer-gender').val(),
        _fakultas_lecturer = $('.lecturer-fakultas').val(),
        
        _name_public = $('.public-name').val(),
        _no_Telp_public = $this.find('.public-no-telp').val(),
        _gender_public = $('.public-gender').val(),

        data_mahasiswa = {
            selectRole: _selectRole,
            name: _name_student,
            nim: _nim_Student,
            gender: _gender_student,
            fakultas: _fakultas_student,
        },
        data_dosen = {
          selectRole: _selectRole,
          name: _name_lecturer,
          nim: _nik_lecturer,
          gender: _gender_lecturer,
          fakultas: _fakultas_lecturer,
      },
        data_public = {
          selectRole: _selectRole,
          name: _name_public,
          no_telp: _no_Telp_public,
          gender: _gender_public
      };
      // console.log('roles',roles);
      if (roles === 'STUDENT') {
        data = data_mahasiswa;
        // console.log('ini submit _selectRole',data);
        if ((data.name && data.nim !== '') && (data.gender && data.fakultas !== null)) {
          if (data.nim.length < 10 || data.nim.length > 10) {
            alert('Mohon Cek Kembali Inputan Anda')
            window.location.reload()              
          } else {
            document.location.href='./page/question/Website_Survey.html';
            localStorage.setItem('data_Personal', JSON.stringify(data))
          }
        } else {
          alert('Mohon Cek Kembali Inputan Anda')
          window.location.reload()
        }
      } else if (roles === 'LECTURER') {
        data = data_dosen;
        if ((data.name && data.nim !== '') && (data.gender && data.fakultas !== null)) {
          if (data.nim.length < 10 || data.nim.length > 10) {
            alert('Mohon Cek Kembali Inputan Anda')
            window.location.reload()              
          } else {
            document.location.href='./page/question/Website_Survey.html';
            localStorage.setItem('data_Personal', JSON.stringify(data))
          }
        } else {
          alert('Mohon Cek Kembali Inputan Anda')
          window.location.reload()
        }
      } else if (roles === 'PUBLIC') {
        data = data_public
        if ((data.name && data.no_telp !== '') && (data.gender !== null)) {
          document.location.href='./page/question/Website_Survey.html';
          localStorage.setItem('data_Personal', JSON.stringify(data))
        } else {
          alert('Mohon Cek Kembali Inputan Anda')
          window.location.reload()
        }
      }
    });
  });