const query = `
query {
    questions {
      id
      code
      content
    }
  }
`,
  url = "https://survey-untirta-backend.ms-prakarsa.id/graphql",
  datas= [],
  opts = {
  method: "POST",
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  },
body: JSON.stringify({ query })
};
fetch(url, opts)
  .then(r => r.json())
  .then(({ data }) => (document.getElementById('id_3').innerHTML = data.questions
    .map(r => `
      <label class="form-label form-label-top form-label-auto" id="${r.id}" for="${r.id}"> ${r.content}  </label>
      <form id=${r.id} class="form-input-wide">
        <fieldset class="selections" id="Pertanyaan-${r.code}" style="border: none; margin-bottom: 1rem;margin-top: 2rem;">
          <label class="container-radio">Sangat Tidak Baik
            <input type="radio" name="radio" value="SANGAT_TIDAK_BAIK" id="${r.id}">
            <span class="checkmark opt-1" name="group"></span>
          </label>
          <label class="container-radio">Tidak Baik
            <input type="radio" name="radio" value="TIDAK_BAIK" id="${r.id}">
            <span class="checkmark opt-2" name="group"></span>
          </label>
          <label class="container-radio">Cukup
            <input type="radio" name="radio" value="CUKUP" id="${r.id}">
            <span class="checkmark opt-3" name="group"></span>
          </label>
          <label class="container-radio opt-3">Baik
            <input type="radio" name="radio" value="BAIK" id="${r.id}">
            <span class="checkmark opt" name="group"></span>
          </label>
          <label class="container-radio">Sangat Baik
            <input type="radio" name="radio" value="SANGAT_BAIK" id="${r.id}">
            <span class="checkmark opt-4" name="group"></span>
          </label>
        </fieldset>
      </form>
    `)
    .join("")
  ));