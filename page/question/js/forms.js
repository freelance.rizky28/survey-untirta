$(document).ready(function() {
    function getChecklistItems() {
        var result =
            $(".selections > .container-radio > input[name='radio']:checked").get();
    
            var columns = $.map(result, function(element) {
                var data ={
                    question: $(element).attr("id"),
                    choosen_option:$(element).val()
                }
                return data;
            });
            // console.log('column',columns)
            return columns
    };

    $('.submit-button').on('click',function (e) {
        e.preventDefault();
        var localData = JSON.parse(localStorage.getItem('data_Personal')),
        s =  localData.fakultas;
        s = s.substring(s.indexOf("(") + 1),
        s = s.substring(0, s.indexOf(")"));
        var the_value,
        the_value = getChecklistItems(),
        data_mahassiswa = {
            group : localData.selectRole, // STUDENT, LECTURER, STAFF, PUBLIC
            faculty : localData.fakultas, // Perlu rundingan untuk membahas list nilai bakunya
            major : s, // Ini juga
            name : localData.name, // Nama submitter
            answer : the_value,
            nim:localData.nim
        },
        data_dosen = {
            group : localData.selectRole, // STUDENT, LECTURER, STAFF, PUBLIC
            faculty : localData.fakultas, // Perlu rundingan untuk membahas list nilai bakunya
            major : s, // Ini juga
            name : localData.name, // Nama submitter
            answer : the_value,
            nip:localData.nim
        },
        data_public = {
            group : localData.selectRole, // STUDENT, LECTURER, STAFF, PUBLIC
            name : localData.name, // Nama submitter
            answer : the_value,
            phone:localData.phone
        };
        // console.log('localData',localData);
        // console.log('datas',data_mahassiswa);
        return fetch('https://survey-untirta-backend.ms-prakarsa.id/submissions', {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            },
            body: JSON.stringify(
                localData.selectRole ==='STUDENT' ? data_mahassiswa :
                localData.selectRole ==='LECTURER' ? data_dosen : 
                localData.selectRole ==='PUBLIC'? data_public :'')
        })
        .then(response => {
            // console.log('berhasil',response)
            if (response.status === 200) {
                alert('Terima Kasih Atas Waktu Anda')
                window.location.href = '../../index.html';
            } else {
                alert('Mohon Maaf Ada Kesalahan')
                window.location.href = '../../index.html';
            }
        })
        .catch(error => {
            console.log('error',error)
        });
    })
})
